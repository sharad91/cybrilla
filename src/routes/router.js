const express = require('express');
const app = express.Router();

let cybrillaDiscount = require('../../components/discount/bin/controller');
app.use('/dashboard/discount', cybrillaDiscount);

let cybrillaCart = require('../../components/cart/bin/controller');
app.use('/dashboard', cybrillaCart);

module.exports = app;
