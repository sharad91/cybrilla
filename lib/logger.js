var fs = require("fs");
var winston = require("winston");
require("winston-daily-rotate-file");
var dateFormat = require("dateformat");
var path = require("path");

if (!fs.existsSync("logs")) {
  fs.mkdirSync("logs");
}

var logger = winston.createLogger({
  transports: [
    new winston.transports.DailyRotateFile({
      name: "file#info",
      level: "info",
      filename: path.join(__dirname + "/../logs/", "log"),
      prepend: true,
      timestamp: function() {
        return dateFormat(new Date(), "dddd, mmmm dS, yyyy, h:MM:ss TT");
      }
    }),
    new winston.transports.DailyRotateFile({
      name: "file#error",
      level: "error",
      filename: path.join(__dirname + "/../logs/", "error"),
      timestamp: function() {
        return dateFormat(new Date(), "dddd, mmmm dS, yyyy, h:MM:ss TT");
      },
      prepend: true,
      handleExceptions: true
    })
  ],
  exitOnError: false
});

module.exports = logger;
