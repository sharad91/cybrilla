$(document).ready(function ($) {
	updateCartCount();

	$('.cartButton').on('click', function () {
		$('#cartVal').html('');
		let productDetails = this.parentElement.id.split(',');
		let data = { product_name: productDetails[0], price: productDetails[1] };
		$.ajax({
			url: BASE_URL + 'dashboard/add/cart',
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function (responseText, data) {
				if (responseText.success) {
					updateCartCount();
				}
			},
		});
	});

	$('#modal-close').on('click', function () {
		$('#myModal').hide();
		$('#backdrop').removeClass('backdrop');
	});

	$('#cartVal').on('click', function () {
		$('#myModal').show();
		$('#backdrop').addClass('backdrop');
		$.ajax({
			url: BASE_URL + 'dashboard/discount/product/list',
			type: 'GET',
			success: function (response) {
				$('#total').html('');
				let html =
					' <div class="modal-content">\
				<span>Product Name</span>\
				<span>Price</span>\
				<span>Quantity</span>\
				<span>Discount</span>\
				<span>Discount Price</span>\
				<span>Total Price</span>\
			</div>';
				for (let i = 0; i < response.data.finalArray.length; i++) {
					let totalPrice = response.data.finalArray[i].total_price ? response.data.finalArray[i].total_price : 0;
					let discountedPrice = response.data.finalArray[i].discounted_price ? response.data.finalArray[i].discounted_price : 0;
					let discount = response.data.finalArray[i].discount ? response.data.finalArray[i].discount : 0;
					html +=
						'<div class="product-list-item" style="padding: 1rem;">\
				<li>' +
						response.data.finalArray[i].name +
						'</li>\
				<li>' +
						response.data.finalArray[i].price +
						'</li>\
					<li>' +
						response.data.finalArray[i].quantity +
						'</li>\
					<li>' +
						discount +
						'</li>\
				<li>' +
						discountedPrice +
						'</li>\
					<li>' +
						totalPrice +
						'</li>\
				</div>';
				}
				$('#total').append(html);
				$('#finalPrice').html('');
				let priceHtml = '';
				if (response.data.total > response.data.basket) {
					priceHtml +=
						'<div>Total Price = ' +
						response.data.total +
						'</div>\
					<div>Basket Price = ' +
						response.data.basket +
						'</div>\
					<div>Discounted Total Price  = ' +
						(response.data.total - response.data.basket) +
						'</div>';
				}
				$('#finalPrice').append(priceHtml);
			},
		});
	});

	function updateCartCount() {
		$.ajax({
			url: BASE_URL + 'dashboard/cart/count',
			type: 'GET',
			success: function (response) {
				let html = '<span>View Cart : </span><span><b>' + response.data.length + '</b></span>';
				$('#cartVal').append(html);
			},
		});
	}
});
