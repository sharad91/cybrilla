require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');
global._LOGGER = require('./lib/logger');
global.BASEURL = process.env.BASEURL;
const app = express();

/** Setting public view directories*/
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/src/views/'));

/**Setting Middleware */
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(bodyParser.json({ limit: '52428800' }));

/**Setting Mongo Connection */
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGO_DB, {
	useUnifiedTopology: true,
	useCreateIndex: true,
	useNewUrlParser: true,
});
app.use(express.static(path.join(__dirname, 'assets')));
app.use(express.static(path.join(__dirname, 'src')));

/**  Routes  */
const router = require('./src/routes/router');
app.use(router);

app.get('/', function (req, res) {
	res.redirect('/dashboard');
});

app.use(function (req, res, next) {
	res.render('error/404');
});

/**Setting Configuration of locals */
app.locals.USER = false;
app.locals._PAGETITLE = false;

module.exports = app;
