let _this = (module.exports = {});
dSchema = require('./schema');

_this.configureDiscount = function (reqBody) {
	return new Promise((resolve, reject) => {
		dSchema.discounts
			.create({
				name: reqBody.product_name,
				quantity: reqBody.quantity,
				price: reqBody.price,
			})
			.then((data) => {
				return resolve(data);
			});
	});
};

_this.basketDiscount = function (reqBody) {
	return new Promise((resolve, reject) => {
		dSchema.discounts
			.create({
				price: reqBody.basket,
			})
			.then((data) => {
				return resolve(data);
			});
	});
};

_this.saveBasketDiscount = function (reqBody) {
	return new Promise((resolve, reject) => {
		dSchema.basket_discount
			.create({
				price: reqBody.basket,
				discount: reqBody.discount,
			})
			.then((data) => {
				return resolve(data);
			});
	});
};

_this.getProductDiscountList = function () {
	return new Promise(async (resolve, reject) => {
		let productNameArray = [];
		let productItemArray = [];
		let finalArray = [];
		let cartList = await pSchema.cart.find();
		let basketDiscount = await dSchema.basket_discount.findOne();

		for (let i = 0; i < cartList.length; i++) {
			if (productNameArray.indexOf(cartList[i].name) === -1) {
				productNameArray.push(cartList[i].name);
			}
		}

		for (let i = 0; i < productNameArray.length; i++) {
			let productName = productNameArray[i];
			let itemfilter = cartList.filter((item) => {
				return item.name === productName;
			});
			productItemArray.push(itemfilter);
		}
		let allProductTotalPrice = 0;
		for (let i = 0; i < productItemArray.length; i++) {
			let TotalProductLength = productItemArray[i].length;
			let pName = productItemArray[i][0].name;
			let price = productItemArray[i][0].price;
			let discountData = await dSchema.discounts.findOne({ name: pName });

			for (let j = 0; j < productItemArray[i].length; j++) {
				let obj = {};
				if (discountData && TotalProductLength >= discountData.quantity) {
					let discountVal = TotalProductLength / discountData.quantity;
					let discountedPrice = price * TotalProductLength - Math.floor(discountVal);
					let totalDiscount = Math.floor(discountVal);
					totalDiscount = totalDiscount * discountData.price;
					discountedPrice = price * TotalProductLength - Math.floor(totalDiscount);

					obj.name = pName;
					obj.price = price;
					obj.quantity = TotalProductLength;
					obj.discount = totalDiscount;
					obj.discounted_price = discountedPrice;
					obj.total_price = price * TotalProductLength;
					allProductTotalPrice += price * TotalProductLength;
					finalArray.push(obj);
					break;
				} else {
					obj.name = pName;
					obj.price = price;
					obj.quantity = TotalProductLength;
					obj.total_price = price * TotalProductLength;
					allProductTotalPrice += price * TotalProductLength;
					finalArray.push(obj);
					break;
				}
			}
		}
		let finalProductPrice = allProductTotalPrice;
		if (basketDiscount) {
			if (allProductTotalPrice > basketDiscount.price) {
				allProductTotalPrice = allProductTotalPrice - basketDiscount.discount;
			}
		}

		return resolve({
			finalArray: finalArray,
			total: finalProductPrice,
			discountPrice: allProductTotalPrice,
			basket: basketDiscount ? basketDiscount.discount : 0,
		});
	});
};
