const mongoose = require('mongoose');

let discountSchema = new mongoose.Schema(
	{
		name: { type: String },
		quantity: { type: Number },
		price: { type: Number },
	},
	{ timestamps: true }
);
const discounts = mongoose.model('discounts', discountSchema);

let basketDiscountSchema = new mongoose.Schema(
	{
		price: { type: String },
		discount: { type: String },
	},
	{ timestamps: true }
);
const basket_discount = mongoose.model('basket_discount', basketDiscountSchema);

module.exports = { discounts, basket_discount };
