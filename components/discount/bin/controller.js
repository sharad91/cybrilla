const express = require('express');
const app = express.Router();
dModel = require('../lib/model');

app.get('/', async function (req, res, next) {
	try {
		res.render('dashboard/discount', {});
	} catch (e) {
		return next(e);
	}
});

app.get('/basket', async function (req, res, next) {
	try {
		res.render('dashboard/basket', {});
	} catch (e) {
		return next(e);
	}
});

app.post('/basket', async function (req, res) {
	try {
		let basketData = await dModel.saveBasketDiscount(req.body);
		res.redirect(301, global.BASEURL + 'dashboard');
	} catch (e) {
		console.log(e.toString());
		_LOGGER.info(e.toString());
	}
});

app.get('/product/list', async function (req, res) {
	try {
		let productPriceData = await dModel.getProductDiscountList();
		res.send({ success: true, data: productPriceData });
	} catch (e) {
		console.log(e.toString());
		_LOGGER.info(e.toString());
	}
});

app.post('/configure', async function (req, res, next) {
	try {
		await dModel.configureDiscount(req.body);
		res.redirect(301, global.BASEURL + 'dashboard');
	} catch (e) {
		return next(e);
	}
});

module.exports = app;
