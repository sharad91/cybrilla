const express = require('express');
const app = express.Router();
pModel = require('../lib/model');

app.get('/', async function (req, res) {
	try {
		let productList = await pModel.getAllProducts();
		res.render('dashboard/index', { response: productList });
	} catch (e) {
		console.log(e.toString());
		_LOGGER.info(e.toString());
	}
});

app.get('/add', async function (req, res, next) {
	try {
		res.render('dashboard/form', {});
	} catch (e) {
		return next(e);
	}
});

app.post('/product', async function (req, res, next) {
	try {
		await pModel.saveProductDetails(req.body);
		res.redirect(301, global.BASEURL + 'dashboard');
	} catch (e) {
		return next(e);
	}
});

app.get('/cart/count', async function (req, res) {
	try {
		let cartCount = await pModel.getCartCount();
		res.send({ success: true, data: cartCount });
	} catch (e) {
		console.log(e.toString());
		_LOGGER.info(e.toString());
	}
});

app.post('/add/cart', async function (req, res) {
	try {
		let cartData = await pModel.saveCartData(req.body);
		res.send({ success: true, data: cartData });
	} catch (e) {
		console.log(e.toString());
		_LOGGER.info(e.toString());
	}
});

module.exports = app;
