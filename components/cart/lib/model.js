let _this = (module.exports = {});
pSchema = require('./schema');

_this.getAllProducts = function () {
	return new Promise(async (resolve, reject) => {
		let products = pSchema.products.find();
		return resolve(products);
	});
};

_this.saveProductDetails = function (reqBody) {
	return new Promise((resolve, reject) => {
		pSchema.products
			.create({
				name: reqBody.product_name,
				price: reqBody.price,
			})
			.then((data) => {
				return resolve(data);
			});
	});
};

_this.saveCartData = function (reqBody) {
	return new Promise(async (resolve, reject) => {
		pSchema.cart
			.create({
				name: reqBody.product_name,
				price: reqBody.price,
			})
			.then((data) => {
				return resolve(true);
			});
	});
};

_this.getCartCount = function () {
	return new Promise(async (resolve, reject) => {
		let cartList = await pSchema.cart.find();
		return resolve(cartList);
	});
};
