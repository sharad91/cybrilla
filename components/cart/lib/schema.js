const mongoose = require('mongoose');

let productsSchema = new mongoose.Schema(
	{
		name: { type: String },
		price: { type: String },
	},
	{ timestamps: true }
);
const products = mongoose.model('products', productsSchema);

let cartSchema = new mongoose.Schema(
	{
		name: { type: String },
		price: { type: String },
	},
	{ timestamps: true }
);
const cart = mongoose.model('cart', cartSchema);

module.exports = { products, cart };
